/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [point]
      ,[date]
	  ,min([out]) as min_s
	  ,max([out]) as max_s
      ,sum([out]) as sum_total
  FROM [labor_sql].[dbo].[outcome]
  group by rollup (point, date)