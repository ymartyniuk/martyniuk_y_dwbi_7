/****** find pc`s related speed and price  ******/
select top 1000 [code]
      ,[model]
      ,[speed]
      ,[ram]
      ,[hd]
      ,[cd]
      ,[price]
  from [labor_sql].[dbo].[pc]
  where speed >= 500 and price >= 800
  order by price asc