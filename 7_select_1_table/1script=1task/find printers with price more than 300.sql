/****** find printers with price < 300  ******/
select [model]
      ,[type]
      ,[price]
  from [labor_sql].[dbo].[printer]
  where price < 300
  order by type desc