/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [maker]
      ,count([model]) as models_count
  FROM [labor_sql].[dbo].[product]
  where type = 'pc'
  group by maker
  having count(model)>1