use people_ua_db;

create clustered index ix_citizens_id on citizens(id)
with (fillfactor=50, pad_index=on);

create nonclustered index ix_m_names_id_name on m_names(id asc, name asc)
with (fillfactor=30, pad_index=on);

create unique index ix_sattlements_sattlement on sattlements(sattlement)
with (fillfactor=20, pad_index=on);