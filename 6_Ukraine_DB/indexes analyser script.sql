use [people_ua_db]
go
select 
	object_name(t1.[object_id]) as 'table', 
		t2.[name] as 'index', 
		t1.[index_id], 
    	t1.[page_count], 
    	t1.[index_type_desc], 
    	t1.[avg_fragmentation_in_percent], 
		t1.[avg_fragment_size_in_pages],
		t1.[avg_page_space_used_in_percent],
    	t1.[fragment_count] 
from  sys.dm_db_index_physical_stats(db_id(), null, null, null, 'sampled') t1 
	inner join sys.indexes t2  
	on  t1.[object_id] = t2.[object_id] and t1. [index_id] = t2. [index_id]
go