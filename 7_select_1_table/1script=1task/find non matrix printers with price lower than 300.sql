/****** find non matrix printers with price < 300$  ******/
select [code]
      ,[model]
      ,[color]
      ,[type]
      ,[price]
  from [labor_sql].[dbo].[printer]
  where type <> 'matrix' and price < 300
  order by type desc