/****** launched ships between 1920 and 1942  ******/
select [name]
		,[launched]
  from [labor_sql].[dbo].[ships]
  where launched between 1920 and 1942
  order by launched desc