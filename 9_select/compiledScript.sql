---task 1---
use labor_sql
go
 ;with cte_product(maker, model, type)
 as (select * from [labor_sql].[dbo].[product])
 select * from [labor_sql].[dbo].[laptop] as l join cte_product on l.model=cte_product.model  

---task 2---
use labor_sql
go
;with 
cte_count_laptops(l_qauntity)
as (select count(*) from [labor_sql].[dbo].[product] where type = 'laptop'),
cte_count_pc(pc_qauntity)
as (select count(*) from [labor_sql].[dbo].[product] where type = 'pc')

select * from cte_count_laptops, cte_count_pc
go

---task 3---
use labor_sql;  
go  
with cte_hier(region_id, id, name, hier_node) as   
(  
    select region_id, id, name, 0 as hier_node  
    from geography   
    where region_id is null  
    union all  
    select e.region_id, e.id, e.name, hier_node + 1  
    from geography as e  
        inner join cte_hier as d  
        on e.region_id = d.id  
)  
select region_id, id, name, hier_node   
from cte_hier  
where hier_node = 1 ;  
go 

---task 4---
use labor_sql
go
;with cte_hier_branch([id] ,[name], [region_id], [node])
as(
select [id]
      ,[name]
      ,[region_id]
	  ,0 as node
  from [labor_sql].[dbo].[geography] where name = 'lviv'
 union all
select g.[id]
      ,g.[name]
      ,g.[region_id]
	  ,g_cte.node + 1
  from [labor_sql].[dbo].[geography] as g
inner join cte_hier_branch as g_cte
on g.region_id = g_cte.id
)
select * from cte_hier_branch
go

---task 5---
use labor_sql
go
;with
cte_num_list
as(
select 1 as row_num
union all
select row_num+1 from cte_num_list where row_num <10000
) 
select * from cte_num_list option(maxrecursion 10000)
go

---task 6---
use labor_sql
go
;with
cte_num_list
as(
select 1 as row_num
union all
select row_num+1 from cte_num_list where row_num <100000
) 
select * from cte_num_list option(maxrecursion 0)
go

---task 7---
use labor_sql
go
;with cte_days(date_indx, day_indx)
as
(
select dateadd(day,0,'2018-01-01') as date_indx, datename(weekday,'2018-01-01') as day_indx
union all
select date_indx+1, datename(weekday, date_indx+1) from cte_days where date_indx < '2018-12-31'
)

select count(*) as saturday_count from cte_days where day_indx = 'saturday' option(maxrecursion 0)

;with cte_days(date_indx, day_indx)
as
(
select dateadd(day,0,'2018-01-01') as date_indx, datename(weekday,'2018-01-01') as day_indx
union all
select date_indx+1, datename(weekday, date_indx+1) from cte_days where date_indx < '2018-12-31'
)

select count(*) as sunday_count from cte_days where day_indx = 'sunday' option(maxrecursion 0)
go

---task 8---
use labor_sql
go
select distinct p.maker
from product as p
where p.[type]='pc' and p.maker not in 
(select maker from product where [type]='laptop')
go

---task 9---
use labor_sql
go
select distinct p.maker
from product as p
where p.[type]='pc' and p.maker <>all 
(select maker from product where [type]='laptop')
go

---task 10---
use labor_sql
go
select distinct p.maker
from product as p
where p.[type]='pc' and not p.maker = any 
(select maker from product where [type]='laptop')
go

---task 11---
use labor_sql
go
select distinct p.maker
from product as p
where p.[type]='pc' and p.maker in 
(select maker from product where [type]='laptop')
go

---task 12---
use labor_sql
go
select distinct p.maker
from product as p
where p.[type]='pc' and not p.maker <> all
(select maker from product where [type]='laptop')
go

---task 13---
use labor_sql
go
select distinct p.maker
from product as p
where p.[type]='pc' and not p.maker <> all
(select maker from product where [type]='laptop')
go

---task 14---
use labor_sql
go
select distinct p.maker
from product as p
where p.[type]='pc' and p.maker not in
(select maker from product where [type]='pc' 
and model<>all(select model from pc))
go

---task 15---
use labor_sql
go
select [country], [class]
  from [labor_sql].[dbo].[classes] c1
  where c1.country <>any(
select [country]
  from [labor_sql].[dbo].[classes] c2
  where c2.country='Ukraine')
go

---task 16---
use labor_sql
go
with cte_battles(ship, battle, date)
as(
SELECT o.[ship] ,o.[battle], b.[date]
  FROM [labor_sql].[dbo].[outcomes] o
  inner join [labor_sql].[dbo].battles b on o.battle=b.name
  where o.result = 'damaged'
  )
select ship, battle, date from cte_battles where ship 
in (SELECT [ship]  FROM [labor_sql].[dbo].[outcomes] where result = 'OK')
go

---task 17---
use labor_sql
go
SELECT distinct dbo.product.maker from dbo.product
where exists 
(SELECT dbo.product.maker, dbo.pc.model
FROM dbo.product
INNER JOIN dbo.pc 
ON dbo.product.model = dbo.pc.model)
go

---task 18---
use labor_sql
go
select distinct [maker]from [labor_sql].[dbo].[product]
where maker in 
(select [maker] from [labor_sql].[dbo].[product]
where model in (select top 1 [model]
from [labor_sql].[dbo].[pc]
order by speed desc))
and type = 'printer'
go

---task 19---
use labor_sql
go
select s.class from outcomes as o
inner join ships as s on o.ship=s.name
where o.result='sunk'
union
select c.class from outcomes as o
inner join classes as c on o.ship=c.class
where o.result='sunk'
go

---task 20---
use labor_sql
go
select [model]
      ,[price]
  from [labor_sql].[dbo].[printer]
  where price in(select max(price) from printer)
go

---task 21---
use labor_sql
go
select p.[type], 
	  l.[model],
      l.[speed]
  from [labor_sql].[dbo].[laptop] as l
  inner join product as p on p.model=l.model
  where speed <all(select [speed]  from [labor_sql].[dbo].[pc])
go

---task 22---
use labor_sql
go
select p.maker, pr.price
  from [labor_sql].[dbo].[printer] as pr
  inner join product as p on p.model = pr.model
  and pr.price in
  (select top 1 [price]
  from [labor_sql].[dbo].[printer] where color = 'y'
  order by price asc)
go

---task 23---
use labor_sql
go
SELECT o.battle, c.country, COUNT(o.ship) AS ship_count
FROM outcomes AS o
INNER JOIN ships AS s
ON o.ship=s.[name]
INNER JOIN classes AS c
ON s.class=c.class
GROUP BY c.country, o.battle
HAVING COUNT(o.ship)>1;
go

---task 24---
use labor_sql
go
select distinct maker ,(select count(model) from pc where model in(select model from product where maker=p.maker)) as pc,
(select count(model) from laptop where model in(select model from product where maker=p.maker)) as laptop,
(select count(model) from printer where model in(select model from product where maker=p.maker)) as printer
from product as p
go

---task 25---
use labor_sql
go
;with cte_result as (
select distinct maker ,
(select count(model) from pc where model in(select model from product where maker=p.maker)) as pc
from product as p)
select maker ,
case 
when pc=0 then 'no'
else 'yes('+cast (pc as varchar(10))+')'
end  pc
from cte_result
go

---task 26---
use labor_sql
go

go

---task 27---
use labor_sql
go

go

---task 28---
use labor_sql
go

go

---task 29---
use labor_sql
go

go

---task 30---
use labor_sql
go

go

---task 32---
use labor_sql
go

go

---task 33---
use labor_sql
go

go
