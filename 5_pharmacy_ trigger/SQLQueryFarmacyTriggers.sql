--TRIGGERS CREATION FOR DELETION--
USE [TRIGER_FK_CURSOR]
GO
CREATE TRIGGER TG_STREET_DEL ON [DBO].[STREET]
AFTER DELETE
AS
BEGIN
DECLARE @INPUTVAL VARCHAR(25)=(SELECT STREET FROM DELETED) 
DECLARE @ROWCOUNT INT = (SELECT COUNT(*) FROM PHARMACY WHERE STREET = @INPUTVAL)
IF (
@ROWCOUNT > 0
)
BEGIN
PRINT 'YOU CAN NOT DELETE REFERENCED RECORD'
ROLLBACK TRANSACTION
END
GO

USE [TRIGER_FK_CURSOR]
GO
CREATE TRIGGER TG_PHARMACY_DEL ON [DBO].[PHARMACY]
AFTER DELETE
AS
BEGIN
DECLARE @INPUTVAL INT = (SELECT ID FROM DELETED) 
DECLARE @ROWCOUNT INT = (SELECT COUNT(*) FROM PHARMACY_MEDICINE WHERE PHARMACY_ID = @INPUTVAL)
IF (
@ROWCOUNT > 0
)
BEGIN
PRINT 'YOU CAN NOT DELETE REFERENCED RECORD'
ROLLBACK TRANSACTION
END
END
GO

USE [TRIGER_FK_CURSOR]
GO
CREATE TRIGGER TG_MEDICINE_DEL ON [DBO].[MEDICINE]
AFTER DELETE
AS
BEGIN
DECLARE @INPUTVAL INT = (SELECT ID FROM DELETED) 
DECLARE @ROWCOUNT INT = (SELECT COUNT(*) FROM PHARMACY_MEDICINE WHERE MEDICINE_ID = @INPUTVAL)
IF (
@ROWCOUNT > 0
)
BEGIN
PRINT 'YOU CAN NOT DELETE REFERENCED RECORD'
ROLLBACK TRANSACTION
END
END
GO

USE [TRIGER_FK_CURSOR]
GO
CREATE TRIGGER TG_PHARMACY_MEDICINE_DEL ON [DBO].[PHARMACY_MEDICINE]
AFTER DELETE
AS
BEGIN
DECLARE @INPUTVAL INT = (SELECT PHARMACY_ID FROM DELETED) 
DECLARE @ROWCOUNT INT = (SELECT COUNT(*) FROM EMPLOYEE WHERE PHARMACY_ID = @INPUTVAL)
IF (
@ROWCOUNT > 0
)
BEGIN
PRINT 'YOU CAN NOT DELETE REFERENCED RECORD'
ROLLBACK TRANSACTION
END
END
GO

USE [TRIGER_FK_CURSOR]
GO
CREATE TRIGGER TG_POST_DEL ON [DBO].[POST]
FOR DELETE
AS
BEGIN
DECLARE @INPUTVAL VARCHAR(15) = (SELECT POST FROM DELETED) 
DECLARE @ROWCOUNT INT = (SELECT COUNT(*) FROM EMPLOYEE WHERE POST = @INPUTVAL)
IF (
@ROWCOUNT > 0
)
BEGIN
PRINT 'YOU CAN NOT DELETE REFERENCED RECORD'
ROLLBACK TRANSACTION
END
END
GO

USE [TRIGER_FK_CURSOR]
GO
CREATE TRIGGER TG_ZONE_DEL ON [DBO].[ZONE]
FOR DELETE
AS
BEGIN
DECLARE @INPUTVAL INT = (SELECT ID FROM DELETED) 
DECLARE @ROWCOUNT INT = (SELECT COUNT(*) FROM MEDICINE_ZONE WHERE ZONE_ID = @INPUTVAL)
IF (
@ROWCOUNT > 0
)
BEGIN
PRINT 'YOU CAN NOT DELETE REFERENCED RECORD'
ROLLBACK TRANSACTION
END
END
GO

USE [TRIGER_FK_CURSOR]
GO
CREATE TRIGGER TG_MEDICINE_ZONE_DEL ON [DBO].[MEDICINE_ZONE]
FOR DELETE
AS
BEGIN
DECLARE @INPUTVAL INT =(SELECT MEDICINE_ID FROM DELETED) 
DECLARE @ROWCOUNT INT = (SELECT COUNT(*) FROM PHARMACY_MEDICINE WHERE MEDICINE_ID = @INPUTVAL)
IF (
@ROWCOUNT > 0
)
BEGIN
PRINT 'YOU CAN NOT DELETE REFERENCED RECORD'
ROLLBACK TRANSACTION
END
END
GO