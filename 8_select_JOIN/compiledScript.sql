---task 1---
use labor_sql
go
select dbo.product.maker, dbo.product.type, dbo.pc.speed, dbo.pc.hd
from dbo.pc inner join dbo.product on dbo.pc.model = dbo.product.model
where hd < 8
go

---task 2---
use labor_sql
go
select dbo.product.maker
from dbo.pc inner join dbo.product on dbo.pc.model = dbo.product.model
where speed >= 600
go

---task 3---
use labor_sql
go
select distinct dbo.product.maker
from dbo.laptop inner join dbo.product on dbo.laptop.model = dbo.product.model
where speed <=500
go

---task 4---
use labor_sql
go
select distinct dbo.laptop.model, laptop_1.model as model_1, dbo.laptop.hd, dbo.laptop.ram
from dbo.laptop inner join dbo.laptop as laptop_1 
on dbo.laptop.ram = laptop_1.ram and dbo.laptop.hd = laptop_1.hd 
order by model, model_1 asc
go

---task 5---
use labor_sql
go
select distinct dbo.classes.country, dbo.classes.type, c.type as type1
from dbo.classes inner join dbo.classes as c 
on dbo.classes.country = c.country
where classes.type = 'bb' and c.type = 'bc'
go

---task 6---
use labor_sql
go
select dbo.product.model, dbo.product.maker
from dbo.product inner join dbo.pc 
on dbo.pc.model = dbo.product.model
where pc.price < 600
go

---task 7---
use labor_sql
go
select dbo.product.model, dbo.product.maker
from dbo.printer inner join dbo.product 
on dbo.printer.model = dbo.product.model
where price > 300
go

---task 8---
use labor_sql
go
select dbo.product.maker, dbo.product.model, dbo.pc.price
from dbo.product inner join dbo.pc 
on dbo.product.model = dbo.pc.model
go

---task 9---
use labor_sql
go
select dbo.product.maker, dbo.product.model, dbo.pc.price
from dbo.product left join dbo.pc 
on dbo.product.model = dbo.pc.model
where product.type = 'pc'
go

---task 10---
use labor_sql
go
select dbo.product.maker, dbo.product.type, dbo.product.model, dbo.laptop.speed
from dbo.product inner join dbo.laptop 
on dbo.product.model = dbo.laptop.model
where laptop.speed > 600
go

---task 11---
use labor_sql
go
select dbo.ships.*, dbo.classes.displacement
from dbo.ships inner join dbo.classes
on dbo.ships.class = dbo.classes.class
go

---task 12---
use labor_sql
go
select dbo.outcomes.battle, dbo.battles.date
from dbo.outcomes inner join dbo.battles 
on dbo.outcomes.battle = dbo.battles.name
where dbo.outcomes.result = 'ok'
go

---task 13---
use labor_sql
go
select dbo.ships.*, dbo.classes.country
from dbo.ships inner join dbo.classes 
on dbo.ships.class = dbo.classes.class
go

---task 14---
use labor_sql
go
select dbo.trip.*, dbo.company.name as comp_name
from dbo.trip inner join dbo.company 
on dbo.trip.id_comp = dbo.company.id_comp
where dbo.trip.plane = 'boeing'
go

---task 15---
use labor_sql
go
select dbo.passenger.*, dbo.pass_in_trip.date
from dbo.passenger inner join dbo.pass_in_trip 
on dbo.passenger.id_psg = dbo.pass_in_trip.id_psg
go

---task 16---
use labor_sql
go
select dbo.product.model, dbo.pc.speed, dbo.pc.hd
from dbo.pc inner join dbo.product on dbo.pc.model = dbo.product.model
where (dbo.pc.hd = 10) or (dbo.pc.hd = 20) and (dbo.product.maker = 'a')
order by speed
go

---task 17---
use labor_sql
go
select * from (select maker, type, model from dbo.product) as data
pivot (count(model) for type in ([pc],[laptop],[printer])) as pvt_table
go

---task 18---
use labor_sql
go
select * from (select screen, price, 'avg_value' avg_pr from dbo.laptop) as data
pivot (avg(price) for screen in ([11],[12],[14],[15])) as pvt_table
go

---task 19---
use [labor_sql]
go

select * 
from [dbo].[laptop] as l
cross apply
(select maker from dbo.product as p where l.model = p.model) as result
go

---task 20---
use [labor_sql]
go

select * 
from [dbo].[laptop] as l
cross apply
(select max(price) as max_price from [dbo].[laptop] as l2 inner join [dbo].[product] as p on l2.model=p.model
where maker in (select maker from dbo.product as p where l.model = p.model)) as result
go

---task 21---
use labor_sql
go

select * from laptop as l0
cross apply
(select top 1 * from laptop as l1
where l0.model<l1.model or (l0.model =l1.model and l0.code<l1.code)
order by model,code) as data 
order by l0.model

go

---task 22---
use labor_sql
go

select * from laptop as l0
outer apply
(select top 1 * from laptop as l1
where l0.model<l1.model or (l0.model =l1.model and l0.code<l1.code)
order by model,code) as data 
order by l0.model

go

---task 23---
use labor_sql
go

select * from (select distinct type from product) as p0
cross apply 
(select top 3 * from product as p1 where p1.type = p0.type order by model) as data

go

---task 24---
use labor_sql
go
SELECT code, name, value FROM laptop as l0
CROSS APPLY
(VALUES('speed', speed), ('ram', ram), ('hd', hd), ('screen', screen)) spec(name, value)
ORDER BY code, name, value
go

