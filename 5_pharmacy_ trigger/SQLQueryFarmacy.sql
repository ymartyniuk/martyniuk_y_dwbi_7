--tables filling--

USE [TRIGER_FK_CURSOR]
GO

INSERT INTO [DBO].[EMPLOYEE]
           ([SURNAME]
           ,[NAME]
           ,[POST]
           )
     VALUES
           ('DO','JOHN','DOCTOR')
GO 10

INSERT INTO [dbo].[MEDICINE]
           ([NAME])
     VALUES
           ('Medicine text')
GO 10

USE [TRIGER_FK_CURSOR]
GO

INSERT INTO [dbo].[ZONE]
           ([NAME])
     VALUES
           ('Somme name')
GO 10

USE [TRIGER_FK_CURSOR]
GO

INSERT INTO [dbo].[MEDICINE_ZONE]
           ([MEDICINE_ID]
           ,[ZONE_ID])
     VALUES
           ('10','10')
GO 

USE [TRIGER_FK_CURSOR]
GO

INSERT INTO [dbo].[PHARMACY]
           ([NAME])
     VALUES
           ('Text1')
GO 5

USE [TRIGER_FK_CURSOR]
GO

begin
while (select max([PHARMACY_ID]) from [dbo].[PHARMACY_MEDICINE]) < 10
INSERT INTO [dbo].[PHARMACY_MEDICINE]
           ([PHARMACY_ID]
           ,[MEDICINE_ID])
     VALUES
           ((select max([PHARMACY_ID]) from [dbo].[PHARMACY_MEDICINE]) +1,
		   (select max([MEDICINE_ID]) from [dbo].[PHARMACY_MEDICINE]) +1)
end

USE [TRIGER_FK_CURSOR]
GO

BEGIN
WHILE (SELECT COUNT(*) FROM [DBO].[POST]) < 10
INSERT INTO [DBO].[POST]
           ([POST])
     VALUES
           (CONCAT('DOCTOR', (SELECT COUNT(*) FROM [DBO].[POST])+1))
END
GO

USE [TRIGER_FK_CURSOR]
GO

BEGIN
WHILE (SELECT COUNT(*) FROM [DBO].[STREET]) < 10
INSERT INTO [DBO].[STREET]
           ([STREET])
     VALUES
           (CONCAT('STREET', (SELECT COUNT(*) FROM [DBO].[STREET])+1))
END
GO