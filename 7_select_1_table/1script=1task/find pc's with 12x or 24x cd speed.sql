/****** find pc`s with 12x or 24x cd speed  ******/
select [model]
      ,[speed]
      ,[hd]
      ,[cd]
      ,[price]
  from [labor_sql].[dbo].[pc]
  where cd in ('12x', '24x')
  order by speed desc