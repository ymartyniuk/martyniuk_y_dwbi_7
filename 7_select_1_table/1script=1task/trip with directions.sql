/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [trip_no]
      ,[id_comp]
      ,[plane]
      ,concat('from',' ',[town_from],' ','to',' ',[town_to]) as direction
      ,[time_out]
      ,[time_in]
  FROM [labor_sql].[dbo].[trip]