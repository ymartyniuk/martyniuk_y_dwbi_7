/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [ship]
      ,[battle]
      ,case result
	  when 'sunk' then 'потоплений'
	  when 'damaged' then 'понищений'
	  when 'ok' then 'на ходу'
	  else ''
	  end as result

  FROM [labor_sql].[dbo].[outcomes]