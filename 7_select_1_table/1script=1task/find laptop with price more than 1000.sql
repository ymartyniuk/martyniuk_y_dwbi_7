/****** find laptop with price > 1000$  ******/
select [model]
      ,[ram]
      ,[screen]
	  ,[price]
  from [labor_sql].[dbo].[laptop]
  where price > 1000
  order by ram asc, price desc