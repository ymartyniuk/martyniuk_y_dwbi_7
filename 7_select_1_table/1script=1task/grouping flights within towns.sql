/****** Script for SelectTopNRows command from SSMS  ******/
SELECT count(trip_no) as flights, town_from, town_to
  FROM [labor_sql].[dbo].[trip] 
  group by grouping sets(town_from, town_to)