/****** incomes with inc > 5000, < 1000  ******/
select [point]
      ,[date]
      ,[inc]
  from [labor_sql].[dbo].[income_o]
  where inc > 5000 and inc < 10000 
  order by inc asc