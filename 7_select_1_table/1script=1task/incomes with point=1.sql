/****** incomes with point=1  ******/
select [code]
      ,[point]
      ,[date]
      ,[inc]
  from [labor_sql].[dbo].[income]
  where point = 1
  order by inc asc