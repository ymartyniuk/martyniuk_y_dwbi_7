----task 01----
use labor_sql
go
select [maker]
      ,[type]
  from [labor_sql].[dbo].[product] where type = 'laptop'
  order by maker asc
  go

  ----task 02----
use labor_sql
go
select [model]
      ,[ram]
      ,[screen]
	  ,[price]
  from [labor_sql].[dbo].[laptop]
  where price > 1000
  order by ram asc, price desc
  go

   ----task 03----
use labor_sql
go
  select [code]
      ,[model]
      ,[color]
      ,[type]
      ,[price]
  from [labor_sql].[dbo].[printer]
  where color = 'y'
  order by price desc
  go

----task 04----
use labor_sql
go
  select [model]
      ,[speed]
      ,[hd]
      ,[cd]
      ,[price]
  from [labor_sql].[dbo].[pc]
  where cd in ('12x', '24x')
  order by speed desc
  go

----task 05 ----
use labor_sql
go
select count([name])
      ,[class]
  from [labor_sql].[dbo].[ships]
  group by class
go

----task 06----
use labor_sql
go
select top 1000 [code]
      ,[model]
      ,[speed]
      ,[ram]
      ,[hd]
      ,[cd]
      ,[price]
  from [labor_sql].[dbo].[pc]
  where speed >= 500 and price >= 800
  order by price asc
go

----task 07----
use labor_sql
go
select [code]
      ,[model]
      ,[color]
      ,[type]
      ,[price]
  from [labor_sql].[dbo].[printer]
  where type <> 'matrix' and price < 300
  order by type desc
go

----task 08----
use labor_sql
go
select [model]
      ,[speed]
  from [labor_sql].[dbo].[pc]
  where price between 400 and 600
  order by hd asc
go

----task 09----
use labor_sql
go
select [model]
      ,[speed]
      ,[hd]
      ,[price]
from [labor_sql].[dbo].[laptop]
where screen >= 12
order by price desc
go

----task 10----
use labor_sql
go
select [model]
      ,[type]
      ,[price]
  from [labor_sql].[dbo].[printer]
  where price < 300
  order by type desc
go

----task 11----
use labor_sql
go
select [model]
      ,[ram]
      ,[price]
from [labor_sql].[dbo].[laptop]
where ram = 64
order by screen asc
go

----task 12----
use labor_sql
go
select [model]
      ,[ram]
      ,[price]
  from [labor_sql].[dbo].[pc]
  where ram > 64
  order by hd asc
go

----task 13----
use labor_sql
go
select [model]
      ,[speed]
      ,[price]
  from [labor_sql].[dbo].[pc]
  where speed > 500 and speed < 750
  order by hd desc
go

----task 14----
use labor_sql
go
select [point]
      ,[date]
      ,[out]
  from [labor_sql].[dbo].[outcome_o]
  where out > 2000
  order by date desc
go

----task 15----
use labor_sql
go
select [point]
      ,[date]
      ,[inc]
  from [labor_sql].[dbo].[income_o]
  where inc > 5000 and inc < 10000 
  order by inc asc
go

----task 16----
use labor_sql
go
select [code]
      ,[point]
      ,[date]
      ,[inc]
  from [labor_sql].[dbo].[income]
  where point = 1
  order by inc asc
go

----task 17----
use labor_sql
go
select [code]
      ,[point]
      ,[date]
      ,[out]
  from [labor_sql].[dbo].[outcome]
  where point=2
  order by out asc
go

----task 18----
use labor_sql
go
select [class]
 from [labor_sql].[dbo].[classes]
  where country = 'japan'
  order by type desc
go

----task 19----
use labor_sql
go
select [name]
		,[launched]
  from [labor_sql].[dbo].[ships]
  where launched between 1920 and 1942
  order by launched desc
go

----task 20----
use labor_sql
go
select [ship]
  from [labor_sql].[dbo].[outcomes]
  where battle = 'guadalcanal' and result <> 'sunk'
go

----task 21----
use labor_sql
go
select [ship]
      ,[battle]
      ,[result]
  from [labor_sql].[dbo].[outcomes]
  where result = 'sunk'
  order by ship desc
go

----task 22----
use labor_sql
go
select [class]
      ,[displacement]
  from [labor_sql].[dbo].[classes]
  where displacement >= 40000
  order by type asc
go

----task 23----
use labor_sql
go
select [trip_no]
      ,[town_from]
      ,[town_to]
 from [labor_sql].[dbo].[trip]
 where town_from = 'london' or town_to = 'london'
 order by time_out asc
go

----task 24----
use labor_sql
go
select [trip_no]
      ,[plane]
      ,[town_from]
      ,[town_to]
  from [labor_sql].[dbo].[trip]
  where plane = 'tu-134'
  order by time_out desc
go

----task 25----
use labor_sql
go
select [trip_no]
      ,[plane]
      ,[town_from]
      ,[town_to]
  from [labor_sql].[dbo].[trip]
  where plane = 'il-86'
  order by plane asc
go

----task 26----
use labor_sql
go
select [trip_no]
      ,[town_from]
      ,[town_to]
from [labor_sql].[dbo].[trip]
where town_from <> 'rostov' or town_to <> 'rostov'
order by plane asc
go

----task 27----
use labor_sql
go
select [model]
  from [labor_sql].[dbo].[pc]
  where model like '%1%1%'
go

----task 28----
use labor_sql
go
select [code]
      ,[point]
      ,[date]
      ,[out]
  from [labor_sql].[dbo].[outcome]
  where month([date]) = 3
go

----task 29----
use labor_sql
go
select [code]
      ,[point]
      ,[date]
      ,[out]
  from [labor_sql].[dbo].[outcome]
  where day([date])= 14
go

----task 30----
use labor_sql
go
select [name]
  from [labor_sql].[dbo].[ships]
  where name like 'w%n'
go

----task 31----
use labor_sql
go
select [name]
  from [labor_sql].[dbo].[ships]
  where name like '%e%e%'
go

----task 32----
use labor_sql
go
select [name]
      ,[launched]
  from [labor_sql].[dbo].[ships]
  where name like '%a'
go

----task 33----
use labor_sql
go
select [name]
  from [labor_sql].[dbo].[battles]
  where name like '% %'
  and name not like '%c'
go

----task 34----
use labor_sql
go
select [trip_no]
      ,[id_comp]
      ,[plane]
      ,[town_from]
      ,[town_to]
      ,[time_out]
      ,[time_in]
  from [labor_sql].[dbo].[trip]
  where time_out > '11:59' and time_in < '17:01'
go

----task 35----
use labor_sql
go
select [trip_no]
      ,[id_comp]
      ,[plane]
      ,[town_from]
      ,[town_to]
      ,[time_out]
      ,[time_in]
  from [labor_sql].[dbo].[trip]
  where time_out >= '17:00' and time_in <= '23:00'
go

----task 36----
use labor_sql
go
select [trip_no]
      ,[id_comp]
      ,[plane]
      ,[town_from]
      ,[town_to]
      ,[time_out]
      ,[time_in]
  from [labor_sql].[dbo].[trip]
  where time_out >= '21:00' and time_in <= '10:00'
go

----task 37----
use labor_sql
go
select [trip_no]
      ,[date]
      ,[id_psg]
      ,[place]
  from [labor_sql].[dbo].[pass_in_trip]
where place like '1%'
go

----task 38----
use labor_sql
go
select [date]
from [labor_sql].[dbo].[pass_in_trip]
  where place like '%c%'
go

----task 39----
use labor_sql
go
select substring([name], (charindex(' ', [name])),100) as surname
  from [labor_sql].[dbo].[passenger]
  where name like '% c%'
go

----task 40----
use labor_sql
go
select substring([name], (charindex(' ', [name])),100) as surname
  from [labor_sql].[dbo].[passenger]
  where name not like '% j%'
go

----task 41----
use labor_sql
go
select concat('середня ціна = ', avg([price]))
  from [labor_sql].[dbo].[laptop]
go

----task 42----
use labor_sql
go
select concat('код:',[code]) as code
      ,concat('модель:',[model]) as model
      ,concat('швидкість:',[speed]) as speed
      ,concat('озп:',[ram]) as ram
      ,concat('жорсткий диск:',[hd]) as hd
      ,concat('ціна:',[price]) as price
  from [labor_sql].[dbo].[pc]
go

----task 43----
use labor_sql
go
select convert(char(10),[date],102) as conv_date 
  from [labor_sql].[dbo].[income]
go

----task 44----
use labor_sql
go
select [ship]
      ,[battle]
      ,case result
	  when 'sunk' then 'потоплений'
	  when 'damaged' then 'понищений'
	  when 'ok' then 'на ходу'
	  else ''
	  end as result
  from [labor_sql].[dbo].[outcomes]
go

----task 45----
use labor_sql
go
select [trip_no]
      ,[date]
      ,[id_psg]
      ,concat('ряд: ',left([place],1)) as line
	  ,concat('місце: ',substring([place],2,1)) as place
  from [labor_sql].[dbo].[pass_in_trip]
go

----task 46----
use labor_sql
go
select [trip_no]
      ,[id_comp]
      ,[plane]
      ,concat('from',' ',[town_from],' ','to',' ',[town_to]) as direction
      ,[time_out]
      ,[time_in]
  from [labor_sql].[dbo].[trip]
go

----task 47----
use labor_sql
go
select concat(left([trip_no],1),right([trip_no],1),left([id_comp],1),right([id_comp],1),left([plane],1),right([plane],1),left([town_from],1),right([town_from],1),left([town_to],1),right([town_to],1),left([time_out],1),right([time_out],1),left([time_in],1),right([time_in],1))
  from [labor_sql].[dbo].[trip]
go

----task 48----
use labor_sql
go
select [maker]
      ,count([model]) as models_count
  from [labor_sql].[dbo].[product]
  where type = 'pc'
  group by maker
  having count(model)>1
go

----task 49----
use labor_sql
go
select count(trip_no) as flights, town_from, town_to
  from [labor_sql].[dbo].[trip] 
  group by grouping sets(town_from, town_to)
go

----task 50----
use labor_sql
go
select count([model]) as qnty_printers, type
  from [labor_sql].[dbo].[printer]
  group by type
go

----task 51----
use labor_sql
go

go

----task 52----
use labor_sql
go
select [model]
      ,count(distinct[model]) model_num
      ,[cd]
      ,count(distinct[cd]) as cd_num
  from [labor_sql].[dbo].[pc]
  group by grouping sets (model,cd)
go

----task 53----
use labor_sql
go
select [point]
      ,[date]
	  ,min([out]) as min_s
	  ,max([out]) as max_s
      ,sum([out]) as sum_total
  from [labor_sql].[dbo].[outcome]
  group by rollup (point, date)
go

----task 54----
use labor_sql
go
select [trip_no]
      ,count(left([place],1)) as line
	  ,count([id_psg]) as psngrs
  from [labor_sql].[dbo].[pass_in_trip]
  group by rollup (trip_no)
go

----task 55----
use labor_sql
go
select count([id_psg])
  from [labor_sql].[dbo].[passenger]
  where substring([name], (charindex(' ', [name]))+1,100) like '[sba]%'
go