/****** find laptops with more than 12 inches screen  ******/
select [model]
      ,[speed]
      ,[hd]
      ,[price]
from [labor_sql].[dbo].[laptop]
where screen >= 12
order by price desc