--1--
use labor_sql
go
select row_number() over(order by trip_no) as num ,trip_no, id_comp from trip
order by id_comp, trip_no 
go

--2--
use labor_sql
go
select row_number() over(partition by id_comp order by id_comp) as num ,trip_no, id_comp from trip
order by id_comp, trip_no 
go

--3--
use labor_sql
go
;with pr_cte
as
(
	select model, color, [type], price, 
	       rank() over(partition by [type] order by price) as price_rank
	from printer
)
select model, color, [type], price
from pr_cte
where price_rank=1;
go

--4--
use labor_sql
go
select maker
from [labor_sql].[dbo].[product]
where [type]='pc'
group by maker
having count(model)>2
go

--5--
use labor_sql
go
select [price]
  from [labor_sql].[dbo].[pc]
  order by price asc offset 2 rows fetch next 1 rows only
go
--6--
use labor_sql
go
  ;with cte_p as
(
select id_psg, substring([name], charindex(' ', [name])+1, 100) as [surname] from passenger
)

select *, ntile(3)over(order by (surname)) as [group] from cte_p
go
go

--7--
use labor_sql
go

go

--8--
use labor_sql
go

go

--9--
use labor_sql
go

go