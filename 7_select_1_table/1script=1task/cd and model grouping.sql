/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [model]
      ,count(distinct[model]) model_num
      ,[cd]
      ,count(distinct[cd]) as cd_num
  FROM [labor_sql].[dbo].[pc]
  group by grouping sets (model,cd)