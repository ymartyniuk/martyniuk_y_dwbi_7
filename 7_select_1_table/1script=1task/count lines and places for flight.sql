/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [trip_no]
      ,count(left([place],1)) as line
	  ,count([id_psg]) as psngrs
  FROM [labor_sql].[dbo].[pass_in_trip]
  group by rollup (trip_no)